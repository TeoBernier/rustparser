#![allow(dead_code)]

use MatchingResult::*;
#[derive(Debug, PartialEq, Eq)]
pub enum MatchingResult {
    Match,
    Complete,
    Reject,
}

#[derive(Debug)]
pub struct ParsePatternError(&'static str);

pub trait MatchChar {
    fn try_match(&mut self, c: char) -> MatchingResult;
    fn reset(&mut self) {}
}

use CharPattern::*;
#[derive(Clone)]
pub enum CharPattern {
    PatternRange(CharRange),
    PatternPlus(CharPlus),
    PatternStar(CharStar),
    PatternSequence(CharSequence),
    PatternUnion(CharUnion),
}

impl MatchChar for CharPattern {
    fn try_match(&mut self, c: char) -> MatchingResult {
        match self {
            PatternRange(x) => x.try_match(c),
            PatternPlus(x) => x.try_match(c),
            PatternStar(x) => x.try_match(c),
            PatternSequence(x) => x.try_match(c),
            PatternUnion(x) => x.try_match(c),
        }
    }

    fn reset(&mut self) {
        match self {
            PatternRange(x) => x.reset(),
            PatternPlus(x) => x.reset(),
            PatternStar(x) => x.reset(),
            PatternSequence(x) => x.reset(),
            PatternUnion(x) => x.reset(),
        }
    }
}

use std::fmt::{self, Debug, Formatter};
impl Debug for CharPattern {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            PatternRange(ref x) => write!(f, "{:?}", x),
            PatternPlus(ref x) => write!(f, "{:?}", x),
            PatternStar(ref x) => write!(f, "{:?}", x),
            PatternSequence(ref x) => write!(f, "{:?}", x),
            PatternUnion(ref x) => write!(f, "{:?}", x),
        }
    }
}

impl CharPattern {
    fn read_rule(
        char_iterator: &mut impl Iterator<Item = char>,
        in_parenthesis: bool,
    ) -> Result<CharPattern, ParsePatternError> {
        let mut sequence: Vec<CharPattern> = Vec::new();
        let mut is_current_union = false;
        while let Some(c) = char_iterator.next() {
            let pattern = match c {
                '[' => {
                    let beg = match char_iterator.next() {
                        Some(c) => c,
                        _ => return Err(ParsePatternError("Error in range pattern : [a-b]")),
                    };
                    match char_iterator.next() {
                        Some('-') => (),
                        _ => return Err(ParsePatternError("Error in range pattern : [a-b]")),
                    }
                    let end = match char_iterator.next() {
                        Some(c) => c,
                        _ => return Err(ParsePatternError("Error in range pattern : [a-b]")),
                    };
                    match char_iterator.next() {
                        Some(']') => (),
                        _ => return Err(ParsePatternError("Error in range pattern : [a-b]")),
                    }
                    Some(PatternRange(CharRange(beg, end, false)))
                }
                '(' => Some(CharPattern::read_rule(char_iterator, true)?),
                ')' => {
                    return if in_parenthesis {
                        match sequence.len() {
                            0 => Err(ParsePatternError("Error, found '()'")),
                            _ => Ok(PatternSequence(CharSequence(sequence, 0))),
                        }
                    } else {
                        Err(ParsePatternError("Error, found ')' without '('"))
                    };
                }
                '*' => match sequence.pop() {
                    Some(pattern) => Some(PatternStar(CharStar(Box::new(pattern), false))),
                    None => {
                        return Err(ParsePatternError(
                            "Error, found '*' with nothing to starify",
                        ))
                    }
                },
                '+' => match sequence.pop() {
                    Some(pattern) => Some(PatternPlus(CharPlus(Box::new(pattern), false))),
                    None => {
                        return Err(ParsePatternError(
                            "Error, found '+' with nothing to plusify",
                        ))
                    }
                },
                '\\' => {
                    let c = match char_iterator.next() {
                        Some('t') => '\t',
                        Some('n') => '\n',
                        Some('r') => '\r',
                        Some('0') => '\0',
                        Some(c) => c,
                        None => {
                            return Err(ParsePatternError(
                                "Error, found '\\' with nothing to escape",
                            ))
                        }
                    };
                    Some(PatternRange(CharRange(c, c, false)))
                }
                '|' => {
                    is_current_union = true;
                    if sequence.is_empty() {
                        return Err(ParsePatternError("Error, found '|' with nothing to orify"));
                    }
                    if !matches!(sequence.last().unwrap(), PatternUnion(..)) {
                        let pattern = PatternUnion(CharUnion(vec![sequence.pop().unwrap()]));
                        sequence.push(pattern);
                    }
                    None
                }
                c => Some(PatternRange(CharRange(c, c, false))),
            };
            if let Some(pattern) = pattern {
                if is_current_union {
                    match sequence.last_mut().unwrap() {
                        PatternUnion(CharUnion(ref mut patterns, ..)) => patterns.push(pattern),
                        _ => unreachable!(),
                    }
                    is_current_union = false;
                } else {
                    sequence.push(pattern);
                }
            }
        }
        if is_current_union {
            Err(ParsePatternError("Error, found '|' with nothing to orify"))
        } else {
            Ok(PatternSequence(CharSequence(sequence, 0)))
        }
    }
}

use std::str::FromStr;
impl FromStr for CharPattern {
    type Err = ParsePatternError;

    fn from_str(s: &str) -> Result<CharPattern, ParsePatternError> {
        if let Some((_, s)) = s.split_once('"') {
            if let Some((s, _)) = s.rsplit_once('"') {
                // println!("{}", s);
                return CharPattern::read_rule(&mut s.chars(), false);
            }
        }
        Err(ParsePatternError(
            "rules has to be shaped like : `rule_name : \"rule\"`",
        ))
    }
}

// Match if the char is in the range else complete (behave like star operator)
#[derive(Debug, Clone)]
pub struct CharRange(char, char, bool);
impl MatchChar for CharRange {
    fn try_match(&mut self, c: char) -> MatchingResult {
        if self.2 {
            Complete
        } else if (self.0 <= c && c <= self.1) || (self.1 <= c && c <= self.0) {
            self.2 = true;
            Match
        } else {
            Reject
        }
    }
    fn reset(&mut self) {
        self.2 = false
    }
}

// Complete if the pattern has matched one time
#[derive(Debug, Clone)]
pub struct CharFirst(Box<CharPattern>, bool);
impl MatchChar for CharFirst {
    fn try_match(&mut self, c: char) -> MatchingResult {
        if self.1 {
            Complete
        } else if self.0.try_match(c) == Match {
            self.1 = true;
            Match
        } else {
            Reject
        }
    }

    fn reset(&mut self) {
        self.0.reset();
        self.1 = false;
    }
}

#[derive(Debug, Clone)]
pub struct CharPlus(Box<CharPattern>, bool);
impl MatchChar for CharPlus {
    fn try_match(&mut self, c: char) -> MatchingResult {
        match self.0.try_match(c) {
            Match => {
                self.1 = true;
                Match
            }
            Complete => {
                self.0.reset();
                if self.1 {
                    match self.0.try_match(c) {
                        Match => Match,
                        Complete => Complete,
                        Reject => Complete,
                    }
                } else {
                    Reject
                }
            }
            Reject => Reject,
        }
    }

    fn reset(&mut self) {
        self.0.reset();
        self.1 = false;
    }
}

#[derive(Debug, Clone)]
pub struct CharStar(Box<CharPattern>, bool);
impl MatchChar for CharStar {
    fn try_match(&mut self, c: char) -> MatchingResult {
        match self.0.try_match(c) {
            Match => {
                self.1 = true;
                Match
            }
            Complete => {
                self.0.reset();
                match self.0.try_match(c) {
                    Match => Match,
                    Complete => Complete,
                    Reject => Complete,
                }
            }
            Reject => {
                if self.1 {
                    Reject
                } else {
                    Complete
                }
            }
        }
    }

    fn reset(&mut self) {
        self.0.reset();
        self.1 = false;
    }
}

#[derive(Debug, Clone)]
pub struct CharSequence(Vec<CharPattern>, usize);
impl MatchChar for CharSequence {
    fn try_match(&mut self, c: char) -> MatchingResult {
        if self.1 == self.0.len() {
            Complete
        } else {
            match self.0[self.1].try_match(c) {
                Match => Match,
                Complete => {
                    self.1 += 1;
                    self.try_match(c)
                }
                Reject => Reject,
            }
        }
    }
    fn reset(&mut self) {
        self.1 = 0;
        self.0.iter_mut().for_each(MatchChar::reset);
    }
}

#[derive(Debug, Clone)]
pub struct CharUnion(Vec<CharPattern>);
impl MatchChar for CharUnion {
    fn try_match(&mut self, c: char) -> MatchingResult {
        //println!("{:?}", self);
        //println!("{:?}", c);
        let res =
            self.0
                .iter_mut()
                .try_fold(Reject, |acc, pattern| match (acc, pattern.try_match(c)) {
                    (Complete, _) => Ok(Complete),
                    (Match, _) => Ok(Match),
                    (Reject, x) => Ok(x),
                });

        //println!("\n{:?}", self);
        //println!("{:?}\n\n", res);

        match res {
            Ok(x) => x,
            Err(x) => x,
        }
    }
    fn reset(&mut self) {
        self.0.iter_mut().for_each(MatchChar::reset);
    }
}

pub struct TokenIter<Iter: Iterator<Item = char>>(Tokenizer, Iter);
impl<Iter: Iterator<Item = char>> Iterator for TokenIter<Iter> {
    type Item = (String, String);
    fn next(&mut self) -> Option<(String, String)> {
        let TokenIter(tokenizer, iter) = self;
        if tokenizer.check_eof(iter) {
            None
        } else {
            match tokenizer.first(iter) {
                Ok(token) => {
                    //println!("{} => {:?}", token.0, token.1);
                    Some(token)
                }
                Err(err) => {
                    if tokenizer.1.is_empty() && self.1.next().is_none() {
                        None
                    } else {
                        panic!("Error while lexing : {:?}", err)
                    }
                }
            }
        }
    }
}

#[derive(Debug)]
pub struct TokenizerError(&'static str);

use std::collections::{HashMap, VecDeque};
#[derive(Debug, Clone)]
pub struct Tokenizer(HashMap<String, CharPattern>, VecDeque<char>);
impl Tokenizer {
    fn from_file(path: &str) -> Self {
        let file = File::open(path).expect("Cannot open file");
        let rules = BufReader::new(file);
        let mut tokenizer = Tokenizer(
            rules
                .lines()
                .map(|s| s.expect("Cannot read file"))
                .filter_map(|s| {
                    let mut iter = s.splitn(2, ':');
                    match (iter.next(), iter.next()) {
                        (Some(a), Some(b)) if !a.starts_with("//") => {
                            Some((a.trim().to_string(), b.trim().parse().unwrap()))
                        }
                        _ => None,
                    }
                })
                .collect(),
            VecDeque::new(),
        );

        assert!(tokenizer.check_empty_word(), "The empty word and `\\0` should not accepted by any rules !\nThose rules are not correct :\n{:#?}", tokenizer.get_incorect_rules());

        tokenizer
    }

    fn reset_patterns(&mut self) {
        self.0
            .iter_mut()
            .for_each(|(_name, pattern)| pattern.reset());
    }

    fn reset(&mut self) {
        self.reset_patterns();
        self.1.clear();
    }

    fn get_incorect_rules(&mut self) -> Vec<&String> {
        self.0
            .iter_mut()
            .filter_map(|(name, pattern)| {
                pattern.reset();
                if matches!(pattern.try_match('\0'), Complete | Match) {
                    pattern.reset();
                    Some(name)
                } else {
                    pattern.reset();
                    None
                }
            })
            .collect()
    }

    fn check_empty_word(&mut self) -> bool {
        self.get_incorect_rules().is_empty()
    }

    fn first(
        &mut self,
        chars: impl Iterator<Item = char>,
    ) -> Result<(String, String), TokenizerError> {
        self.reset_patterns();
        let mut max = None;
        let idx_deque = self.1.len();
        let mut possible_patterns = self.0.iter_mut().collect::<Vec<_>>();
        //println!("{:?}", possible_patterns);

        if !self.1.is_empty() {
            for (idx, &c) in self.1.iter().enumerate() {
                let (new_patterns, complete) = possible_patterns.into_iter().fold(
                    (Vec::new(), None),
                    |(mut vec, complete), rule| match rule.1.try_match(c) {
                        Match => {
                            vec.push(rule);
                            (vec, complete)
                        }
                        Complete => (vec, Some(rule)),
                        Reject => (vec, complete),
                    },
                );
                //println!("{:?}", new_patterns);
                //println!("{:?}", complete);
                if let Some(rule) = complete {
                    max = Some((idx, rule.0.clone()));
                }

                possible_patterns = new_patterns;
                if possible_patterns.is_empty() {
                    break;
                }
            }
            if possible_patterns.is_empty() {
                if let Some((idx, name)) = max {
                    return Ok((name.to_string(), self.1.drain(0..idx).collect::<String>()));
                }
            }
        }

        for (idx, c) in chars.enumerate() {
            self.1.push_back(c);
            let (new_patterns, complete) = possible_patterns.into_iter().fold(
                (Vec::new(), None),
                |(mut vec, complete), rule| match rule.1.try_match(c) {
                    Match => {
                        vec.push(rule);
                        (vec, complete)
                    }
                    Complete => (vec, Some(rule)),
                    Reject => (vec, complete),
                },
            );
            //println!("{:?}", new_patterns);
            //println!("{:?}", complete);
            if let Some(rule) = complete {
                max = Some((idx_deque + idx, rule.0.clone()));
            }

            //println!("\nmax: {:?}\n", max);
            possible_patterns = new_patterns;
            if possible_patterns.is_empty() {
                break;
            }
        }

        if let Some((idx, name)) = max {
            let res = (name.to_string(), self.1.drain(0..idx).collect());
            // println!("{} : \"{}\"", res.0, res.1);

            Ok(res)
        } else {
            // println!("{:?}", self.1);
            Err(TokenizerError("No pattern matching."))
        }
    }

    fn check_eof(&mut self, chars: &mut impl Iterator<Item = char>) -> bool {
        if self.1.is_empty() || self.1[0] == '\0' {
            match chars.next() {
                Some('\0') => self.1.is_empty(),
                Some(c) => {
                    self.1.push_back(c);
                    false
                }
                None => true,
            }
        } else {
            false
        }
    }

    fn tokenize(&self, chars: impl Iterator<Item = char>) -> TokenIter<impl Iterator<Item = char>> {
        TokenIter(self.clone(), chars.chain("\0".chars()))
    }

    fn tokenize_file(&self, path: &str) -> TokenIter<impl Iterator<Item = char>> {
        self.tokenize(read_chars(path))
    }
}

use std::convert::TryFrom;
use std::io::{self, BufRead, BufReader, Bytes, Read};
use std::iter::Map;

const CONT_MASK: u8 = 0b0011_1111;

#[inline]
fn utf8_first_byte(byte: u8, width: u32) -> u32 {
    (byte & (0x7F >> width)) as u32
}

/// Returns the value of `ch` updated with continuation byte `byte`.
#[inline]
fn utf8_acc_cont_byte(ch: u32, byte: u8) -> u32 {
    (ch << 6) | (byte & CONT_MASK) as u32
}

#[inline]
fn unwrap_or_0(opt: Option<u8>) -> u8 {
    match opt {
        Some(byte) => byte,
        None => 0,
    }
}

/// Reads the next code point out of a byte iterator (assuming a
/// UTF-8-like encoding).
#[inline]
pub fn next_code_point<'a, I: Iterator<Item = u8>>(bytes: &mut I) -> Option<u32> {
    // Decode UTF-8
    let x = bytes.next()?;
    if x < 128 {
        return Some(x as u32);
    }

    // Multibyte case follows
    // Decode from a byte combination out of: [[[x y] z] w]
    // NOTE: Performance is sensitive to the exact formulation here
    let init = utf8_first_byte(x, 2);
    let y = unwrap_or_0(bytes.next());
    let mut ch = utf8_acc_cont_byte(init, y);
    if x >= 0xE0 {
        // [[x y z] w] case
        // 5th bit in 0xE0 .. 0xEF is always clear, so `init` is still valid
        let z = unwrap_or_0(bytes.next());
        let y_z = utf8_acc_cont_byte((y & CONT_MASK) as u32, z);
        ch = init << 12 | y_z;
        if x >= 0xF0 {
            // [x y z w] case
            // use only the lower 3 bits of `init`
            let w = unwrap_or_0(bytes.next());
            ch = (init & 7) << 18 | utf8_acc_cont_byte(y_z, w);
        }
    }

    Some(ch)
}

use std::fs::File;
#[derive(Debug)]
pub struct CharsIterator<F: FnMut(io::Result<u8>) -> u8> {
    iterator: Map<Bytes<BufReader<File>>, F>,
    line_offset: usize,
    char_offset: usize,
}

impl<F: FnMut(io::Result<u8>) -> u8> CharsIterator<F> {
    pub fn new(path: &str, f: F) -> Self {
        Self {
            iterator: BufReader::new(File::open(path).expect("Cannot open file"))
                .bytes()
                .map(f),
            line_offset: 0,
            char_offset: 0,
        }
    }

    fn iter_inner(&mut self) -> Option<char> {
        match next_code_point(&mut self.iterator)
            .map(|bytes| TryFrom::<u32>::try_from(bytes).expect("Cannot read file"))
        {
            Some('\n') => {
                self.line_offset += 1;
                self.char_offset = 0;
                Some('\n')
            }
            some @ Some(..) => {
                self.char_offset += 1;
                some
            }
            None => None,
        }
    }
}

impl<F: FnMut(io::Result<u8>) -> u8> Iterator for CharsIterator<F> {
    type Item = char;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter_inner()
    }
}

pub fn read_chars(path: &str) -> CharsIterator<impl FnMut(io::Result<u8>) -> u8> {
    CharsIterator::new(path, |result| result.expect("Problem reading file"))
}

fn main() {
    let tokenizer = Tokenizer::from_file("C/c_rules");

    // println!("{:#?}", tokenizer);

    println!(
        "{:?}",
        tokenizer
            .tokenize_file("C/test_tokenize.c")
            .collect::<Vec<_>>()
    );
}

/*
=> [("IDENT", "int"), ("SEPARATOR", " "), ("IDENT", "main"), ("LPAR", "("), ("IDENT", "void"), ("RPAR", ")"), ("SEPARATOR", " "), ("LBRACK", "{"),
    ("SEPARATOR", "\n\t"), ("IDENT", "int"), ("SEPARATOR", " "), ("IDENT", "a"), ("SEPARATOR", " "), ("OP_AFFECT", "="), ("SEPARATOR", " "),
    ("VAL_HEXA", "0xFFFF"), ("SEMICOL", ";"), ("SEPARATOR", "\n\t"), ("IDENT", "int"), ("SEPARATOR", " "), ("IDENT", "b"), ("SEPARATOR", " "),
    ("OP_AFFECT", "="), ("SEPARATOR", " "), ("VAL_INT", "25"), ("SEMICOL", ";"), ("SEPARATOR", "\n\t"), ("IDENT", "b"), ("SEPARATOR", " "),
    ("OP_BAND_EQ", "&="), ("SEPARATOR", " "), ("IDENT", "a"), ("SEMICOL", ";"), ("SEPARATOR", "\n\t"), ("IDENT", "printf"), ("LPAR", "("),
    ("STRING", "\"%d\""), ("COMMA", ","), ("SEPARATOR", " "), ("IDENT", "b"), ("RPAR", ")"), ("SEMICOL", ";"), ("SEPARATOR", "\n"), ("RBRACK", "}")]
*/
